package zad2;

public class ReportMaker implements Visitor{
    public int products;
    public String report;
    public double discounts;
    
    public ReportMaker() {
        products = 0;
        discounts = 0;
    }
    
    public double getNumberOfDiscounts() {
        return discounts;
     }
    
    public int getNumberOfProducts() { 
        return products;
    }
    public String getReport() {
        return report;
    }
    
    @Override
    public void visit(Visitable visitable) {
        this.report += "\n" + visitable.giveReport();
        
        switch(visitable.getClass().getSimpleName()) {
            case "Product": this.products++; break;
            case "Discount": this.discounts++; break;
        }
    }   
}