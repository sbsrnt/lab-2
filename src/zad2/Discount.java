package zad2;

import java.util.List;

public class Discount implements Visitable {
	public double discount10;
    public double price;
    List<Product> products;
    
    public Discount(List<Product> products, int discount10) {
        this.products = products;
        this.discount10 = discount10;
    }
    
    public double getDiscount10(List<Product> products){
    	if(products.size() > 5)
    	{
    		discount10 = price - (price * 0.1);
    	}
		return discount10;
    }
  
    public double getPrice() {
        return this.price;
    }
    
    public void addProduct(Product product) {
        this.products.add(product);
        this.price += product.price;
    }
       @Override
    public String toString() {
        String str = "";
        
        str += "Discount: " + this.discount10 + ", price: " + this.price + "\nProducts:";
        for(Product product : this.products)
            str += "\n\t" + product.toString();
        
        return str;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String giveReport() {
        return this.toString();
    }
    
}
