package zad2;

import java.util.ArrayList;
import java.util.List;

public class Bucket {
	Product p1 = new Product("Lego", 100);
    System.out.println(p1.toString());
    
    ReportMaker rM = new ReportMaker();
    rM.visit(p1);
    System.out.println(rM._report);
    
    List<Product> l = new ArrayList<>();
      l.add(p1);
      l.add(new Product("Woda"));
      
    Discount d1 = new Discount(l, 0);
    rM.visit(d1);
    System.out.println(rM._report);
	}
}
