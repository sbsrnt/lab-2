package zad2;

import java.util.List;

public class Promotion implements Visitable {
	public double promotion;
    public double price;
    List<Product> products;
    
    public Promotion(List<Product> products, int promotion) {
        this.products = products;
        this.promotion = promotion;
    }
    
    public double getPromotion(List<Product> products){
    	if(products.size() == 3)
    	{
    		promotion = products.size()-1;
    	}
		return promotion;
    }
  
    public double getPrice() {
        return this.price;
    }
    
    public void addProduct(Product product) {
        this.products.add(product);
        this.price += product.price;
    }
       @Override
    public String toString() {
        String str = "";
        
        str += "Promotion: " + this.promotion + ", price: " + this.price + "\nProducts:";
        for(Product product : this.products)
            str += "\n\t" + product.toString();
        
        return str;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String giveReport() {
        return this.toString();
    }
}