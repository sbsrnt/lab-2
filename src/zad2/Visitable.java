package zad2;

interface Visitable{
	public void accept(Visitor visitor);
	public String giveReport();
}
