package zad2;

public class Product implements Visitable{
		public String name;
		public double price;
		
		public Product(String name, double price){
			this.name = name;
			this.price = price;
		}
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public double getPrice() {
			return price;
		}
		public void setPrice(double price) {
			this.price = price;
		}
		
		public void accept(Visitor visitor) {
		        visitor.visit(this);
	    }


	    public String giveReport() {
	        return this.toString();
	    }
	    
	    public String toString() {
	        String str = "";
	        
	        str += "Product: " + this.name + ", price: " + this.price + " .";
	        
	        return str;
	    }
}
